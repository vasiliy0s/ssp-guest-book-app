(function () {
  'use strict';
  
  /**
   * 'app.comment-form' module for Comment' view.
   */
  
  angular.module('app.comment-form', []);
  
} ());
