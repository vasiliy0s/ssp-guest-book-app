(function () {
  'use strict';
  
  /**
   * Comment model is a POJO with simple methods.
   */
  
  angular.module('app.comments')
    .factory('Comment', factory);
  
  function factory (_) {
    
    // Comment model constructor
    function Comment (data) {
      
      // Fill model with default fields and passed data.
      angular.extend(this, {
        _id: void 0,
        text: '',
        authorName: '',
        likes: 0,
        createdAt: (new Date()).toString(),
      }, data);
      
    }
    
    // Comment model methods.
    angular.extend(Comment.prototype, {
      
      // Assign data 
      assign: function (data) {
        _.assign(this, data);
        return this;
      },
      
      // Increment .likes.
      like: function () {
        this.likes++;
        return this;
      },
      
      // Decrement .likes.
      dislike: function () {
        this.likes--;
        return this;
      },
      
      // Check for item is not saved on server.
      isNew: function () {
        return angular.isUndefined(this._id);
      },
      
      // Check for model was updated after create.
      isUpdatedAfterCreate: function () {
        var createdAt = new Date(this.createdAt);
        var updatedAt = new Date(this.updatedAt);
        return updatedAt.getTime() > createdAt.getTime();
      },
      
    });
    
    return Comment;
  }
  
} ());
