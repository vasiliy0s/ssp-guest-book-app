(function () {
  'use strict';
  
  angular.module('app.comments')
    .config(config);
  
  function config ($routeProvider) {
    $routeProvider
      .when('/comments', {
        templateUrl: 'app/comments/comments.html',
        controller: 'CommentsController',
        controllerAs: 'comments',
      });
  }
  
} ());
