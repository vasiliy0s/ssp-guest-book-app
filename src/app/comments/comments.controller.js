(function () {
  'use strict';
  
  /**
   * CommentsController is a controller for 'comments' view.
   */
  
  angular.module('app.comments')
    .controller('CommentsController', CommentsController);
  
  var COMMENTS_VIEWING_LIMIT = 10;
  
  function CommentsController (Comments, Comment, APP_NAME, $scope, CommentsBackend, _, $log, Flash) {
    var vm = this;
    
    $scope.APP_NAME = APP_NAME;

    // Available sorting filers.
    var SORTERS = [
      { expr: 'createdAt', title: 'Create date', },
      { expr: 'likes', title: 'Likes', },
    ];
    
    
    /**
     * Private methods.
     */
    
    // Controller activation.
    function activate () {
      vm.list = Comments;
      
      vm.sorters = SORTERS;
      vm.selectedSorter = '0';
      vm.sortASC = false;
      
      // Load comments from server on initialization.
      loadComments();
      
      vm.add();
    }
    
    // Load comments with offset.
    var loadComments  = _.debounce(function (offset) {
      if (vm.in('loading')) {
        return;
      }
      
      var unsetLoading = vm.set('loading');
      
      var promise = CommentsBackend
        .index({ limit: COMMENTS_VIEWING_LIMIT, offset: offset || 0 });
      
      promise.then(
        _.bind(Comments.add, Comments)
        , headefyErrorAlert('Error on comments loading')
      );
    
      promise.finally(unsetLoading);
      
      return promise;
    }, 500);
    
    // Log and show error alert;
    function errorAlert (err) {
      $log.error(err);
      return Flash.create('danger', err);
    }
    
    // Prefixy error with heading.
    function headefyErrorAlert (prefix) {
      return function (err) {
        return errorAlert('<h4>' + prefix + '</h4>' + err);
      };
    }
    
    
    /**
     * Simple contextual State Machine realization.
     */
   
    vm.$states = [];
    
    // Set current view state.
    vm.set = function (state, comment) {
      vm.$states.push(vm.some(state, comment));
      
      return function unset () {
        return vm.unset(state, comment);
      };
    };
    
    // Unset any or custom state.
    vm.unset = function (state, comment) {
      if (!state) {
        vm.$states.length = 0;
      }
      
      else if (vm.in(state, comment)) {
        _.pull(vm.$states, vm.some(state, comment));
      }
    };
    
    // Check for view in state.
    vm.in = function (state, comment) {
      return vm.$states.indexOf(vm.some(state, comment)) >= 0;
    };
    
    // Build prefixed 'someliking' state for comment separation;
    vm.some = function (prefix, comment) {
      return '' + prefix + (comment ? (':' + comment._id) : '');
    };
    
    
    /**
     * Main controller actions.
     */
    
    // Create'n'edit comment.
    vm.add = function () {
      var comment = new Comment();
      vm.edit(comment);
      vm.newComment = comment;
    };
    
    // Edit comment.
    vm.edit = function (comment) {
      comment.$editing = true;
    };
    
    // Check for comment is editing.
    vm.isEditing = function (comment) {
      return true === comment.$editing;
    };
            
    // Cancel edit.
    vm.cancel = function (comment) {
      comment.$editing = false;
    };
    
    // Overloaded comment saving.
    vm.save = function (comment) {
      var wasNew = comment.isNew();
      var method = wasNew ? 'create' : 'update';
      
      var unsetSaving = vm.set('saving', comment);
      
      CommentsBackend[method](comment)
        .then(function (passed) {
          unsetSaving(); // fix for new comments;
          
          comment.assign(passed);
          vm.cancel(comment);
          
          if (wasNew) {
            Comments.push(comment);
            vm.add();
          }
        }, headefyErrorAlert('Error on comment saving'))
        .finally(unsetSaving);
    };
    
    // Delete comment.
    vm.delete = function (comment) {
      if (comment.isNew()) {
        Comments.remove(comment);
        vm.cancel(comment);
      }

      else {
        var unsetDeleting = vm.set('deleting', comment);
        
        CommentsBackend.delete(comment)
          .then(function () {
            vm.cancel(comment);
            Comments.remove(comment);
          }, headefyErrorAlert('Error on comment deleting'))
          .finally(unsetDeleting);
      }
    };

    // 'Like' comment.
    vm.like = function (comment) {
      return vm.somelike(comment, false);
    };
    
    // 'Dislike' comment.
    vm.dislike = function (comment) {
      return vm.somelike(comment, true);
    };
    
    // Set like/dislike of the comment.
    vm.somelike = function (comment, dislike) {
      var method = dislike === true ? 'dislike' : 'like';

      var unsetSomelikingComment = vm.set('liking', comment);

      // 'Future' action for view responsibility.
      comment[method]();
      
      CommentsBackend[method](comment)
        .then(function () {
          // Update comment with real data.
          CommentsBackend.get(comment)
            .then(function (data) {
              comment.assign(data);
            }, errorAlert)
            .finally(unsetSomelikingComment);
        }, function (err) {
          headefyErrorAlert('Error on comment ' + (method === 'like' ? 'liking' : 'disliking'))(err);
          unsetSomelikingComment();
        });
    };
    
    // Listen event which comments list scrolled to footer.
    $scope.$on('appCommentsList:scrolledToFooter', function () {
      var offset = Comments.length;
      
      loadComments(offset);
    });

    activate();
  }
  
} ());
