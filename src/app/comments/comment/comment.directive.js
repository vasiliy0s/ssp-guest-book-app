(function () {
  'use strict';
  
  angular.module('app.comment')
    .directive('appComment', directive);
    
  function directive () {
    return {
      restrict: 'A',
      replace: true,
      templateUrl: 'app/comments/comment/comment.html',
    };
  }
  
} ());
