(function () {
  'use strict';
  
  /**
   * 'app.comment' module for Comment' view.
   */
  
  angular.module('app.comment', []);
  
} ());
