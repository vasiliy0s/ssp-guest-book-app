(function () {
  'use strict';
  
  /**
   * 'app.comments' module is builded around Comment model with view, collection,
   * RESTful service, etc.
   */
  
  angular.module('app.comments', [
    'ngRoute',
    'app.comment',
    'app.comment-form',
    'flash',
  ]);
  
} ());
