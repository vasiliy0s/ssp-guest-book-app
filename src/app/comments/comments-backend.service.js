(function () {
  'use strict';
  
  /**
   * CommentsBackend service as client' wrapper for Comments RESTful server.
   */
  
  angular.module('app')
    .factory('CommentsBackend', factory);
  
  function factory ($q, $http, BACKEND_HOST) {

    // Constructor.
    function CommentsBackend () {}
    
    // Create scoped path to comments controller actions.
    var buildPath = function (path) {
      return BACKEND_HOST + '/comments' + path;
    };
    
    // Parse object for real id.
    var getRealIdFrom = function (object) {
      if ('string' === typeof object) {
        return object; // it's real ID;
      }
      
      var id = object._id || object.id;
      if ('string' !== typeof id) {
        throw 'CommentsBackend.getRealIdFrom -- cannot get reald item id from object ' + object;
      }
      
      return id;
    };
    
    var buildErrorFrom = function (response) {
      return '' + response.status + ': ' + (response.data || response.statusText || 'Unknown network error');
    };
    
    
    /**
     * Static API.
     */
    angular.extend(CommentsBackend, {
      
      // Get comments collection with options.
      index: function (options) {
        var deferred = $q.defer();
        
        $http
          .get(buildPath('/'), {
            params: options,
          })
          .then(function (res) {
            deferred.resolve(res.data);
          }, function (err) {
            deferred.reject(buildErrorFrom(err));
          });
        
        return deferred.promise;
      },
      
      // Create comment with data.
      create: function (data) {
        var deferred = $q.defer();
        
        $http
          .post(buildPath('/'), data)
          .then(function (res) {
            deferred.resolve(res.data);
          }, function (err) {
            deferred.reject(buildErrorFrom(err));
          });
        
        return deferred.promise;
      },
      
      // Get single comment by id.
      get: function (comment) {
        var deferred = $q.defer();
        var _id = getRealIdFrom(comment);
        
        $http
          .get(buildPath('/' + _id))
          .then(function (res) {
            deferred.resolve(res.data);
          }, function (err) {
            deferred.reject(buildErrorFrom(err));
          });
        
        return deferred.promise;
      },
      
      // Update comment by id with new data.
      update: function (comment, data) {
        var _id = getRealIdFrom(comment);
        var _data = data || comment;
        
        var deferred = $q.defer();
        
        $http
          .patch(buildPath('/' + _id), _data)
          .then(function (res) {
            deferred.resolve(res.data);
          }, function (err) {
            deferred.reject(buildErrorFrom(err));
          });
        
        return deferred.promise;
      },
      
      // Remove comment from server.
      delete: function (comment) {
        var deferred = $q.defer();
        
        var _id = getRealIdFrom(comment);
        
        $http
          .delete(buildPath('/' + _id))
          .then(deferred.resolve, function (err) {
            deferred.reject(buildErrorFrom(err));
          });
        
        return deferred.promise;
      },
      
      // Call 'like' API method for comment.
      like: function (comment) {
        return this.somelike(comment, false);
      },
      
      // Call 'dislike' API method for comment.
      dislike: function (comment) {
        return this.somelike(comment, true);
      },
      
      // Call 'like'/'dislike' API method for comment.
      somelike: function (comment, dislike) {
        var deferred = $q.defer();
        
        var _id = getRealIdFrom(comment);
        var method = dislike === true ? 'dislike' : 'like';
        
        $http
          .patch(buildPath('/' + _id + '/' + method), {})
          .then(deferred.resolve, function (err) {
            deferred.reject(buildErrorFrom(err));
          });
          
        return deferred.promise;
      },
      
    });
    
    return CommentsBackend;
  }

} ());
