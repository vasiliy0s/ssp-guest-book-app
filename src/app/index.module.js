(function () {
  'use strict';
  
  angular.module('app', [
    // App modules
    'app.comments',
    
    // 3rd party modules.
    'ngRoute',
  ]);
  
} ());
