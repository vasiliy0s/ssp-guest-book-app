(function () {
  'use strict';
  
  /**
   * appName directive replce element' content with APP_NAME constant value.
   *
   * Usage:
   *  <div app-name>replacable text</div> 
   */
  
  angular.module('app')
    .directive('appName', directive);
  
  function directive (APP_NAME) {
    return {
      restrict: 'A',
      link: function (scope, element) {
        element.text(APP_NAME);
      }
    };
  }
    
} ());
