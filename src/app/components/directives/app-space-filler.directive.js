(function () {
  'use strict';
  
  /**
   * appSpaceFiller is a Bootstrap-based directive for fill horizontal space.
   *
   * Usage:
   *  <app-space-filler></app-space-filler>
   *  <div app-space-filler></div>
   */
  
  angular.module('app')
    .directive('appSpaceFiller', directive);
  
  function directive () {
    return {
      restrict: 'AE',
      template: [
        '<div class="row">',
          '<h1><!-- space fillter --></h1>',
        '</div>',
      ].join(''),
    };
  }
  
} ());
