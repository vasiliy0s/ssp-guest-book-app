(function (_) {
  'use strict';
  
  angular.module('app')
    .factory('_', factory);
  
  function factory () {
    return _;
  }

/*globals _*/
} (_));
